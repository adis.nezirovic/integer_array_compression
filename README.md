# Integer Array Compression

Playground for testing the integer array compression (simple-8b)

## Description
This project helps exploring simple-8b encoding for packing integer arrays into unsigned 64bit
integers (variable length arrays). The ideas presented here are often used by Time series databases
(Gorilla paper by Facebook, ClickHouse, TimescaleDB)

The original paper for this technique is "Index compression using 64-bit words", Softw. Pract. Exper. 2010; 40:131–147"
Some useful links:ž
- https://onlinelibrary.wiley.com/doi/epdf/10.1002/spe.9https://arxiv.org/pdf/1209.2137.pdf48
- Decoding billions of integers per second through vectorization - https://arxiv.org/pdf/1209.2137.pdf
- https://www.timescale.com/blog/time-series-compression-algorithms-explained/

## Installation
To build this project you need a recent Rust version. `cargo run` will start local HTTP server on
port 8000.

## Usage
The idea is simple, you submit an array of integers via HTTP, and this program shall return the
original array with simple-8b encoded array. Incoming data should be either unsigned 32bit integers
or unsigned 64bit integers (not greater than 2^60).
For example, we can pretend that input data is time offsets in (milli)seconds. And that output is
encoded/compressed info.

Example:

```
curl -s -X POST localhost:8000 -d '[10, 20, 30, 40, 50, 120, 270, 1230]'
{"orig":[10,20,30,40,50,120,270,1230],"compressed":[11664378053452124170,12682136550680355086]}
```

Original data is an array of 8 integers:
- When stored as char array, with separator, it uses 27 bytes
- If we store it as array of 16bit integers, it uses 32 bytes
- If we store it in variable length array (simple-8b encoded), it uses 16 bytes (two)
