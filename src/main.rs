#[macro_use]
extern crate rocket;

use rocket::serde::{json, json::Json, Serialize};

#[derive(Serialize)]
#[serde(crate = "rocket::serde")]
struct CompressionInfo {
    orig: Vec<u64>,
    compressed: Vec<u64>,
}

#[post("/", data = "<cues>")]
fn compress(cues: Json<Vec<u64>>) -> String {
    let orig = cues.to_vec();
    let compressed = &mut vec![];

    let mut total_size = 0;
    let mut r: u64 = 0;

    loop {
        let size = match simple8b::pack(&orig[total_size..], &mut r) {
            Ok(size) => size,
            Err(e) => return e.to_string(),
        };

        total_size += size;
        if r != 0 {
            compressed.push(r);
            r = 0;
        }

        if total_size >= orig.len() {
            break;
        }
    }

    let info = CompressionInfo {
        orig,
        compressed: compressed.to_vec(),
    };

    json::to_string(&info).unwrap()
}

#[launch]
fn rocket() -> _ {
    rocket::build().mount("/", routes![compress])
}
